import { Selector } from "testcafe";

class CustomSelector extends Selector {
  constructor (init=null, options=null, expectedText=null, mandatory=null){
    super(init, options)
    this.expectedText = expectedText
  }

  get textContent() {
    return this.textContent
  }
}

class MyElement {
  constructor(locator, expectedValue) {
    this.element = locator
    this.expectedValue = expectedValue
  }
}

export default CustomSelector