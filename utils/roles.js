import { Role } from 'testcafe';
import login from '../pageModels/login'
import data from '../config'

const adminUser = Role(login.url, async t => {
    await login.auth(`${data.defaultAdminEmail}`, `${data.defaultAdminPassword}`);
    // await t.wait(10000);
}, { preserveUrl: true})

export default adminUser;