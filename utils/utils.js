import {ClientFunction} from 'testcafe'

export function getCurrentMonthName() {
    const date = new Date();
    return date.toLocaleString('default', { month: 'long' });
}

export const getURL = ClientFunction(() => document.location.href);

