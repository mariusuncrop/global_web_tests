import { t, RequestHook } from "testcafe";

class CustomRequestHook extends RequestHook {
  constructor() {
    super(null, {
      includeHeaders: true,
      includeBody: true
    });
    this.requests = {};
    this.failures = [];
  }

  onRequest(event) {
    this.requests[event._requestInfo.requestId] = event._requestInfo;
  }

  async onResponse(event) {
    if (event.statusCode >= 400) {
      this.failures.push({
        url: this.requests[event.requestId].url,
        statusCode: event.statusCode,
        body: event.body.toString('utf8')
      });
    }
  }
}

export default CustomRequestHook