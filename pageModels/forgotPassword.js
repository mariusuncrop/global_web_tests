import { Selector, t } from 'testcafe';
import data from '../config'
import Base from './base'

class ForgotPassword extends Base{
    constructor() {
        super()
        this.url = `${data.login_url}`;
        this.title = Selector('.auth-div b').addCustomMethods({
            getExpectedValue: () => ({
                cat: 'Forgot your password?',
                de: 'Forgot your password?',
                en: 'Forgot your password?',
                es: 'Forgot your password?',
                fr: 'Forgot your password?',
                it: 'Forgot your password?',
                jp: 'Forgot your password?',
                pt: 'Forgot your password?'
            }),
            isMandatory: () => true
        })
        this.subtitle = Selector('.auth-div p').addCustomMethods({
            getExpectedValue: () => ({
                cat: 'Enter your email address and we will send you an email with a link to reset your password',
                de: 'Enter your email address and we will send you an email with a link to reset your password',
                en: 'Enter your email address and we will send you an email with a link to reset your password',
                es: 'Enter your email address and we will send you an email with a link to reset your password',
                fr: 'Enter your email address and we will send you an email with a link to reset your password',
                it: 'Enter your email address and we will send you an email with a link to reset your password',
                jp: 'Enter your email address and we will send you an email with a link to reset your password',
                pt: 'Enter your email address and we will send you an email with a link to reset your password'
            })
        })
        this.input = Selector(".auth-div input[type='email']");
        this.reset = Selector('.auth-div button.btn');
        this.login = Selector('.auth-div a');      
        this.done = Selector('.auth-div > .text-center').addCustomMethods({
            getExpectedValue: () => ({
                cat: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                de: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                en: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                es: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                fr: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                it: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                jp: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page",
                pt: "Done! Please check your email account, you'll receive an email with a reset password linkTo login page"
            })
        });  
    }
}

export default new ForgotPassword();