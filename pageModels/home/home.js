import { Selector, t } from 'testcafe';
import data from '../../config'
import Base from '../base';

class PendingActions {
    constructor (){
        this.parent = Selector('div.panel-adminme').nth(0)
        this.title = this.parent.find('div.panel-title .pull-left')
    }
}

class LastLogins {
    constructor (){
        this.parent = Selector('div.panel-adminme').nth(1)
        this.title = this.parent.find('div.panel-title .pull-left').addCustomMethods({
            isMandatory: () => true
        })
    }
}

class LastDownloadedFiles {
    constructor (){
        this.parent = Selector('div.panel-adminme').nth(2)
        this.title = this.parent.find('div.panel-title .pull-left')
    }
}

class Home extends Base {
    constructor() {
        super();
        this.url = `${data.url}/home`;
        this.uploadFile = Selector('a.btn-info[href="/payroll/uploaded-files/new"]').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Upload a file',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        });
        this.newJoiner = Selector('.row a[href="/team/employees/new-joiner"]').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'New joiner',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.pendingActions = new PendingActions()
        this.lastLogins = new LastLogins()
        this.lastDownloadedFiles = new LastDownloadedFiles()

    }
}

export default new Home();
