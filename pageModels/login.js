import { Selector, t } from 'testcafe';
import data from '../config'
import Base from './base';

class Email {
    constructor() {
        this.parent = Selector('.input-group').nth(0)
        this.icon = this.parent.find('span.fa');
        this.input = this.parent.find('input');
        this.error_message = this.parent.nextSibling('.help-block').addCustomMethods({
            getExpectedValue: () => ({
                cat: 'Please provide Email',
                de: 'Please provide Email',
                en: 'Please provide Email',
                es: 'Please provide Email',
                fr: 'Please provide Email',
                it: 'Please provide Email',
                jp: 'Please provide Eメール',
                pt: 'Please provide Email'
            })
        });
    }
}

class Password {
    constructor() {
        this.parent = Selector('.input-group').nth(1)
        this.icon = this.parent.find('span.fa');
        this.input = this.parent.find('input');
        this.error_message = this.parent.nextSibling('.help-block').addCustomMethods({
            getExpectedValue: () => ({
                cat: 'Please provide Password',
                de: 'Please provide Passwort',
                en: 'Please provide Password',
                es: 'Please provide Contraseña',
                fr: 'Please provide Mot de passe',
                it: 'Please provide Password',
                jp: 'Please provide パスワード',
                pt: 'Please provide Palavra-passe'
            })
        });
    }
}

class Login extends Base {
    constructor() {
        super();
        this.url = `${data.url}/login`;
        this.email = new Email()
        this.password = new Password()
        this.submit = Selector('.auth-div button.btn').addCustomMethods({
            getExpectedValue: () => ({cat: 'Log in', de: 'Einloggen', en: 'Login', es: 'Log In', fr: 'Log in', it: 'Log in', jp: 'ログインする', pt: 'Log in'}),
            isMandatory: () => true
        })
        this.forgotPassword = Selector('.auth-div .white-underlined-anchor').addCustomMethods({
            getExpectedValue: () => ({
                cat: 'Forgot your password?',
                de: 'Passwort vergessen?',
                en: 'Forgot your password?',
                es: '¿Olvidaste la contraseña?',
                fr: 'Mot de passe oublié?',
                it: 'Hai dimenticato la password?',
                jp: 'パスワードをお忘れですか？',
                pt: 'Esqueceu a sua palavra-passe?'
            }),
            isMandatory: () => true
        })
    }

    async auth(email, password) {
        await t
            .typeText(this.email.input, email)
            .typeText(this.password.input, password)
            .click(this.submit)
    }
}

export default new Login();
