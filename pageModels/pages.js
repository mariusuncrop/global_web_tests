import * as myself from './myself/index'
import * as myTeam from './myTeam/index'
import * as payroll from './payroll/index'
import * as timeAndExpenses from './timeAndExpenses/index'
import * as accounts from './accounts/index'
import * as customerService from './customerService/index'
import * as legalEntity from './legalEntity/index'
import * as client from './client/index'
import * as legislation from './legislation/index'
import * as crm from './crm/index'
import * as reportingAndAnalysis from './reportingAndAnalysis/index'
import * as helpAndDocumentation from './helpAndDocumentation/index'
import * as configuration from './configuration/index'

export { default as base } from './base'
export { default as login } from './login'
export { default as forgotPassword } from './forgotPassword'
export { default as myCalendar } from './myself/myCalendar'
export { default as home } from './home/home'
export { myself as myself }
export {myTeam as myTeam}
export {payroll as payroll}
export {timeAndExpenses as timeAndExpenses}
export {accounts as accounts}
export {customerService as customerService}
export {legalEntity as legalEntity}
export {client as client}
export {legislation as legislation}
export {crm as crm}
export {reportingAndAnalysis as reportingAndAnalysis}
export {helpAndDocumentation as helpAndDocumentation}
export {configuration as configuration}
