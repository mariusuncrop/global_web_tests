import { Selector, t } from 'testcafe';
import data from '../config'
import * as utils from '../utils/utils'

class Base {
    constructor() {
        this.header = {
            triggerSidebar: Selector('.navbar .sidebar-toggle'),
            logo: Selector('.navbar img'),
            userName: Selector('.navbar .navbar-custom-menu ul li > span'),
            settings: Selector('.navbar .navbar-custom-menu ul li > a[href="/user-settings"]').addCustomMethods({
                getExpectedValue: () => ({
                    cat: 'Settings',
                    de: 'Settings',
                    en: 'Settings',
                    es: 'Settings',
                    fr: 'Settings',
                    it: 'Settings',
                    jp: 'Settings',
                    pt: 'Settings'
                })
            }),
            logout: Selector('.navbar .navbar-custom-menu ul li > a[role="button"]').addCustomMethods({
                getExpectedValue: () => ({
                    cat: 'Logout',
                    de: 'Logout',
                    en: 'Logout',
                    es: 'Logout',
                    fr: 'Logout',
                    it: 'Logout',
                    jp: 'Logout',
                    pt: 'Logout'
                })
            })
        }
        this.sidebar = {
            home: Selector('ul.sidebar-menu li > a[href="/home"]').addCustomMethods({
                getExpectedValue: () => ({
                    cat: 'Home', de: 'Srartseite', en: 'Home', es: 'Inicio', fr: 'Accueil', it: 'Home', jp: '家', pt: 'Início'
                })
            }),
            myself: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Myself'),
                myDocuments: Selector('ul.sidebar-menu li > a[href="/myself/documents"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Documents',
                        de: 'Meine Dokumente',
                        en: 'My Documents',
                        es: 'Mis Documentos',
                        fr: 'My Documents',
                        it: 'I Miei Documenti',
                        jp: '私の文書',
                        pt: 'Documentos'
                    })
                }),
                myPersonalData: Selector('ul.sidebar-menu li > a[href="/myself/personal"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: "My Personal Data",
                        de: "Personenbezogene Daten",
                        en: "My Personal Data",
                        es: "Mis Datos Personales",
                        fr: "My Personal Data",
                        it: "Dati Personali",
                        jp: "私の個人情報",
                        pt: "Dados Pessoais"
                    })
                }),
                myObjectives: Selector('ul.sidebar-menu li > a[href="/myself/objectives"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Objectives',
                        de: 'Meine Objektive',
                        en: 'My Objectives',
                        es: 'Mis Objetivos',
                        fr: 'My Objectives',
                        it: 'I Miei Obiettivi',
                        jp: 'My Objectives',
                        pt: 'Objetivos'
                    })
                }),
                myReceivedRequests: Selector('ul.sidebar-menu li > a[href="/myself/electronical-mutations"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Received Requests',
                        de: 'My Received Requests',
                        en: 'My Received Requests',
                        es: 'Mis Solicitudes Recibidas',
                        fr: 'My Received Requests',
                        it: 'My Received Requests',
                        jp: 'My Received Requests',
                        pt: 'My Received Requests'
                    })
                }),
                myActivityReport: Selector('ul.sidebar-menu li > a[href="/myself/activity-report"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Activity Report',
                        de: 'My Activity Report',
                        en: 'My Activity Report',
                        es: 'Mi Informe De Actividades',
                        fr: 'My Activity Report',
                        it: 'Le Mie Attivitá',
                        jp: 'My Activity Report',
                        pt: 'Relatório Atividade'
                    })
                }),
                myClockings: Selector('ul.sidebar-menu li > a[href="/myself/clocking"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Clockings',
                        de: 'Meine Arbeitszeiten',
                        en: 'My Clockings',
                        es: 'Mis Fichajes',
                        fr: 'My Clockings',
                        it: 'Il Mio Registro Orario',
                        jp: 'マイクロッキング',
                        pt: 'Fichas De Ponto'
                    })
                }),
                myCalendar: Selector('ul.sidebar-menu li > a[href="/myself/calendar2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Calendar',
                        de: 'Mein Kalender',
                        en: 'My Calendar',
                        es: 'Mi Calendario',
                        fr: 'My Clockings',
                        it: 'Il Mio Calendario',
                        jp: 'My Clockings',
                        pt: 'Calendário'
                    })
                }),
                myLoggedHours: Selector('ul.sidebar-menu li > a[href="/myself/log-hours"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Logged Hours',
                        de: 'My Logged Hours',
                        en: 'My Logged Hours',
                        es: 'My Logged Hours',
                        fr: 'My Logged Hours',
                        it: 'My Logged Hours',
                        jp: 'My Logged Hours',
                        pt: 'My Logged Hours'
                    })
                }),
                myPlanning: Selector('ul.sidebar-menu li > a[href="/myself/planning"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Planning',
                        de: 'Meine Plannung',
                        en: 'My Planning',
                        es: 'Mi Horario',
                        fr: 'My Planning',
                        it: 'Le Mie Pianificazioni',
                        jp: 'My Planning',
                        pt: 'Horário'
                    })
                }),
                myTimesheet: Selector('ul.sidebar-menu li > a[href="/myself/my-time-sheet"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Time-sheet',
                        de: 'My Time-Blatt',
                        en: 'My Time-sheet',
                        es: 'Mi Control Horario',
                        fr: 'My Time-sheet',
                        it: 'Controllo Orario',
                        jp: 'My Time-sheet',
                        pt: 'Ficha De Ponto',
                    })
                }),
                myExpenses: Selector('ul.sidebar-menu li > a[href="/myself/expenses"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'My Expenses',
                        de: 'Meine Ausgaben',
                        en: 'My Expenses',
                        es: 'Mis Gastos',
                        fr: 'My Expenses',
                        it: 'Le Mie Spese',
                        jp: 'My Expenses',
                        pt: 'Despesas'
                    })
                })
            },
            myTeam: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('My Team'),
                employeesFolders: Selector('ul.sidebar-menu li > a[href="/team/employee-folders"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Employees Folders',
                        de: 'Mitarbeiter Ordner',
                        en: 'Employees Folders',
                        es: 'Carpetas De Empleados',
                        fr: 'Fiches Collaborateurs',
                        it: 'Schede Dipendenti',
                        jp: '従業員のフォルダ',
                        pt: 'Pastas Colaboradores'
                    })
                }),
                employeesLists: Selector('ul.sidebar-menu li > a[href="/team/employee-lists"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Employees Lists',
                        de: 'Mitarbeiter Listen',
                        en: 'Employees Lists',
                        es: 'Lista De Empleados',
                        fr: 'Listes Colllaborateurs',
                        it: 'Liste Dipendenti',
                        jp: '従業員リスト',
                        pt: 'Lista Colaboradores',
                    })
                }),
                teamOverview: Selector('ul.sidebar-menu li > a[href="/team/overview"]').addCustomDOMProperties({
                    getExpectedValue: () => ({
                        cat: 'Team Overview',
                        de: 'Team Overview',
                        en: 'Team Overview',
                        es: 'Team Overview',
                        fr: 'Team Overview',
                        it: 'Team Overview',
                        jp: 'Team Overview',
                        pt: 'Team Overview',
                    })
                }),
                directAbsences: Selector('ul.sidebar-menu li > a[href="/team/direct-absences"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Direct Absences',
                        de: 'Absenzen',
                        en: 'Direct Absences',
                        es: 'Ausencias Directas',
                        fr: 'Direct Absences',
                        it: 'Assenze Dirette',
                        jp: '直接欠席',
                        pt: 'Ausências Diretas'
                    })
                }),
                compensation: Selector('ul.sidebar-menu li > a[href="/team/compensations"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Compensation',
                        de: 'Lohnarten',
                        en: 'Compensation',
                        es: 'Compensación',
                        fr: 'Salaires',
                        it: 'Compensazione',
                        jp: '補償',
                        pt: 'Compensações'
                    })
                }),
                actionCenterOld: Selector('ul.sidebar-menu li > a[href="/team/action-center"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Action Center (Old)',
                        de: 'Aktionszentrum (alt)',
                        en: 'Action Center (Old)',
                        es: 'Centro De Actividad (old)',
                        fr: 'Action Center (Old)',
                        it: 'Centro Attivitá (old)',
                        jp: 'Action Center (Old)',
                        pt: 'Centro Ação',
                    })
                }),
                actionCenter: Selector('ul.sidebar-menu li > a[href="/team/action-center2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Action Center',
                        de: 'Aktionszentrum',
                        en: 'Action Center',
                        es: 'Centro De Actividad',
                        fr: 'Action Center',
                        it: 'Centro Attivitá',
                        jp: 'Action Center',
                        pt: 'Centro Ação'
                    })
                }),
                activityReport: Selector('ul.sidebar-menu li > a[href="/team/activity-report"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Activity Report',
                        de: 'Tätigkeitsbericht',
                        en: 'Activity Report',
                        es: 'Informe De Actividades',
                        fr: 'Activity Report',
                        it: 'Attivitá',
                        jp: 'Activity Report',
                        pt: 'Relatório Atividade',
                    })
                }),
                teamCalendar: Selector('ul.sidebar-menu li > a[href="/team/calendar2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Team Calendar',
                        de: 'Teamkalender',
                        en: 'Team Calendar',
                        es: 'Calendario Del Equipo',
                        fr: 'Team Calendar',
                        it: 'Calendario Del Team',
                        jp: 'Team Calendar',
                        pt: 'Calendário Equipa',
                    })
                }),
                teamMutations: Selector('ul.sidebar-menu li > a[href="/team/mutations"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Team Mutations',
                        de: 'Team Mutations',
                        en: 'Team Mutations',
                        es: 'Team Mutations',
                        fr: 'Team Mutations',
                        it: 'Team Mutations',
                        jp: 'Team Mutations',
                        pt: 'Team Mutations',
                    })
                }),
                expenseInvoiced: Selector('ul.sidebar-menu li > a[href="/team/expense-invoiced"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Expense Invoiced',
                        de: 'Expense Invoiced',
                        en: 'Expense Invoiced',
                        es: 'Expense Invoiced',
                        fr: 'Expense Invoiced',
                        it: 'Expense Invoiced',
                        jp: 'Expense Invoiced',
                        pt: 'Expense Invoiced'
                    })
                }),
                absenceDetails: Selector('ul.sidebar-menu li > a[href="/team/absence-details2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Absence Details',
                        de: 'Abwesenheitsinformationen',
                        en: 'Absence Details',
                        es: 'Detalles De Ausencias',
                        fr: 'Absence Details',
                        it: 'Dettagli Assenze',
                        jp: 'Absence Details',
                        pt: 'Absentismo',
                    })
                }),
                vacationDetails: Selector('ul.sidebar-menu li > a[href="/team/vacation-details2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Vacation Details',
                        de: 'Genaue Angaben Zum Aufenthalt',
                        en: 'Vacation Details',
                        es: 'Detalles De Vacaciones',
                        fr: 'Vacation Details',
                        it: 'Dettagli Ferie',
                        jp: 'Vacation Details',
                        pt: 'Detalhes Férias'
                    })
                }),
                countersDetails: Selector('ul.sidebar-menu li > a[href="/team/counters-details2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Counters Details',
                        de: 'Arbeitszeitzähler Details',
                        en: 'Counters Details',
                        es: 'Detalle Contador',
                        fr: 'Counters Details',
                        it: 'Dettagli Conteggio Ore',
                        jp: 'Counters Details',
                        pt: 'Detalhes Contagem'
                    })
                }),
                objectives: Selector('ul.sidebar-menu li > a[href="/team/objectives"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Objectives',
                        de: 'Ziele',
                        en: 'Objectives',
                        es: 'Objetivos',
                        fr: 'Objectives',
                        it: 'Obbiettivi',
                        jp: 'Objectives',
                        pt: 'Objetivos'
                    })
                }),
                pendingApprovals: Selector('ul.sidebar-menu li > a[href="/team/pending-approvals-2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Pending Approvals',
                        de: 'Ausstehende Genehmigungen',
                        en: 'Pending Approvals',
                        es: 'Pendiente De Aprobación',
                        fr: 'Pending Approvals',
                        it: 'Approvazioni In Sospeso',
                        jp: 'Pending Approvals',
                        pt: 'Aprovação Pendente'
                    })
                }),
                expenses: Selector('ul.sidebar-menu li > a[href="/team/expenses/receipts"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: 'Expenses',
                        de: 'Ausgaben',
                        en: 'Expenses',
                        es: 'Gastos',
                        fr: 'Expenses',
                        it: 'Spese',
                        jp: '経費',
                        pt: 'Despesas',
                    })
                })
            },
            payroll: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Payroll'),
                payrollRuns: Selector('ul.sidebar-menu li > a[href="/payroll/runs2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Runs',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                payrollInput: Selector('ul.sidebar-menu li > a[href="/payroll/py-input"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Input',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                payrollOutput: Selector('ul.sidebar-menu li > a[href="/payroll/output2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Output',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                payrollOutput_: Selector('ul.sidebar-menu li > a[href="/payroll/output"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Output_',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                payrollDeclaration: Selector('ul.sidebar-menu li > a[href="/payroll/py-declaration"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Declaration',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                icpCenter: Selector('ul.sidebar-menu li > a[href="/payroll/icp-center"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'ICP Center',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                payrollSignOff: Selector('ul.sidebar-menu li > a[href="/payroll/py-sign-off"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Sign Off',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                payrollControls: Selector('ul.sidebar-menu li > a[href="/payroll/py-controls"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Payroll Controls',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                checkList: Selector('ul.sidebar-menu li > a[href="/payroll/checklist"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Check List',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                taxRates: Selector('ul.sidebar-menu li > a[href="/payroll/tax-rates"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Tax Rates',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                uploadedFiles: Selector('ul.sidebar-menu li > a[href="/payroll/uploaded-files"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Uploaded Files',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                interfaceFiles: Selector('ul.sidebar-menu li > a[href="/payroll/interface-files"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Interface Files',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                calculationLogs: Selector('ul.sidebar-menu li > a[href="/payroll/output/calculation-logs"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Calculation Logs',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                calculationOutput: Selector('ul.sidebar-menu li > a[href="/payroll/output/calculation"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Calculation Output',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                accountingOutput: Selector('ul.sidebar-menu li > a[href="/payroll/output/accounting"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Accounting Output',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                paySubperiodOutput: Selector('ul.sidebar-menu li > a[href="/payroll/output/pay-subperiod"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Pay Subperiod Output',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                employeeTaxOutput: Selector('ul.sidebar-menu li > a[href="/payroll/output/employee-taxes"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Employee Tax Output',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                ptOutputBases: Selector('ul.sidebar-menu li > a[href="/payroll/output/pt-bases"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'PT Output Bases',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                inputInterface: Selector('ul.sidebar-menu li > a[href="/payroll/input-interface"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Input Interface',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                modulesManager: Selector('ul.sidebar-menu li > a[href="/payroll/modules-manager"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Modules Manager',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            timeAndExpenses: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Time & Expenses'),
                sftpConnections: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/sftp-connections/list"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'SFT Connections',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                timeGroupsSetup: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/time-groups-setup2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Time Groups Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                timeApprovers: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/approvers2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Time Approvers',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                countersSetup: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/counters-setup2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Counters Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                absencesSetup: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/absences-presences-setup2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Absences / Presences Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                calendarsSetup: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/calendars-setup2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Calendars Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                closedPeriods: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/closed-periods2"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Closed Periods',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                expenseGroupSetup: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/expense-group-setup"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Expense Group Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                expenseApprovers: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/te-approvers"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Expense Approvers',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                clockingSetup: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/clocking-setup"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Clocking Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                timeMailConfig: Selector('ul.sidebar-menu li > a[href="/time-and-attendance/mails-config"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Time Mail Config',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            accounts: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Accounts'),
                expenses: Selector('ul.sidebar-menu li > a[href="/accounts/acc-expenses"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Expenses',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                billing: Selector('ul.sidebar-menu li > a[href="/accounts/billing"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Billing',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                transactions: Selector('ul.sidebar-menu li > a[href="/accounts/transactions"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Transactions',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                transactionsSetup: Selector('ul.sidebar-menu li > a[href="/accounts/transactions-setup"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Transactions Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                reportGeneration: Selector('ul.sidebar-menu li > a[href="/accounts/report-generation"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Report Generation',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                exchangeSetup: Selector('ul.sidebar-menu li > a[href="/accounts/exchange-setup"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Exchange Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                cashCall: Selector('ul.sidebar-menu li > a[href="/accounts/cash-call"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Cash Call',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            customerService: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Customer Service'),
                ticketingSystem: Selector('ul.sidebar-menu li > a[href="/customer-service/ticketing-system"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Ticketing System',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            legalEntity: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Legal Entity'),
                legalEntityFolders: Selector('ul.sidebar-menu li > a[href="/company/le-folders"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Legal Entity Folders',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                legalEntityLists: Selector('ul.sidebar-menu li > a[href="/company/le-lists"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Legal Entity Lists',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                insurances: Selector('ul.sidebar-menu li > a[href="/company/insurances"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Insurances',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                historicalFilesUpload: Selector('ul.sidebar-menu li > a[href="/company/historicalfilesupload"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Historical Files Upload',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                multipleFilesUpload: Selector('ul.sidebar-menu li > a[href="/company/bulkfilesupload"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Multiple Files Upload',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                migrationFilesUpload: Selector('ul.sidebar-menu li > a[href="/company/migration-files"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Migration Files Upload',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            client: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Client'),
                clientFolders: Selector('ul.sidebar-menu li > a[href="/client/folders"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Client Folders',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                clientLists: Selector('ul.sidebar-menu li > a[href="/client/lists"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Client Lists',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            legislation: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Legislation'),
                legislationFolders: Selector('ul.sidebar-menu li > a[href="/legislation/leg-folders"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Legislation Folders',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                legislationLists: Selector('ul.sidebar-menu li > a[href="/legislation/leg-lists"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Legislation Lists',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                insurances: Selector('ul.sidebar-menu li > a[href="/legislation/insurances/"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Insurances',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                setup: Selector('ul.sidebar-menu li > a[href="/legislation/setup"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Setup',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            crm: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('CRM'),
                accounts: Selector('ul.sidebar-menu li > a[href="/crm/accounts"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Accounts',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                opportunities: Selector('ul.sidebar-menu li > a[href="/crm/opportunities"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Opportunities',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                activities: Selector('ul.sidebar-menu li > a[href="/crm/activities"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Activities',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                stakeholders: Selector('ul.sidebar-menu li > a[href="/crm/stakeholders"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Stakeholders',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                scopes: Selector('ul.sidebar-menu li > a[href="/crm/scopes"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Scopes',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                seoTargets: Selector('ul.sidebar-menu li > a[href="/crm/targets"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'SEO Targets',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                seoAchievement: Selector('ul.sidebar-menu li > a[href="/crm/achievement"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'SEO Achievement',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                commercialDocuments: Selector('ul.sidebar-menu li > a[href="/crm/sales-material"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Commercial Documents',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            reportingAndAnalysis: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Reporting & Analysis'),
                databasesDownload: Selector('ul.sidebar-menu li > a[href="/reporting-and-analysis/databases-download"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Databases Download',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                usersManagAndControl: Selector('ul.sidebar-menu li > a[href="/reporting-and-analysis/users-manag-control"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Users Manag. & Control',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            helpAndDocumentation: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Help & Documentation').addCustomMethods({
                    isMandatory: () => true
                }),
                policiesAndTraining: Selector('ul.sidebar-menu li > a[href="/help/policies"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Policies & Training',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                manualsAndGuidelines: Selector('ul.sidebar-menu li > a[href="/help/manuals"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Manuals & Guidelines',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                operationalDocuments: Selector('ul.sidebar-menu li > a[href="/help/operational"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Operational Documents',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                manageFiles: Selector('ul.sidebar-menu li > a[href="/help/manage-files"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Manage Files',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                securityManagement: Selector('ul.sidebar-menu li > a[href="/help/security-and-signing/list"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Security Management',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            },
            configuration: {
                button: Selector('ul.sidebar-menu li > a[role="button"]').withText('Configuration'),
                tags: Selector('ul.sidebar-menu li > a[href="/configuration/tags"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Tags',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                logs: Selector('ul.sidebar-menu li > a[href="/configuration/logs"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Logs',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                users: Selector('ul.sidebar-menu li > a[href="/configuration/users"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Users',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                roles: Selector('ul.sidebar-menu li > a[href="/configuration/roles"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Roles',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                wageTypes: Selector('ul.sidebar-menu li > a[href="/configuration/wagetypes"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Wage Types',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                worker: Selector('ul.sidebar-menu li > a[href="/configuration/worker"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Worker',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                mutations: Selector('ul.sidebar-menu li > a[href="/configuration/mutations"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Mutations',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                notifications: Selector('ul.sidebar-menu li > a[href="/configuration/notifications"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Notifications',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                parameters: Selector('ul.sidebar-menu li > a[href="/configuration/parameters"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Parameters',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                }),
                logsByEmployee: Selector('ul.sidebar-menu li > a[href="/configuration/logs-by-employee"]').addCustomMethods({
                    getExpectedValue: () => ({
                        cat: '',
                        de: '',
                        en: 'Logs By Employee',
                        es: '',
                        fr: '',
                        it: '',
                        jp: '',
                        pt: '',
                    })
                })
            }
        }
        this.language = Selector('ul.languages li');
        this.toasts = {
            error: {
                title: Selector('.toastr .rrt-title'),
                text: Selector('.toastr .rrt-text')
            }
        }
    }
    /**
     * @desc checks that all page model elements that are marked as mandatory are visible on the page 
     * @param {Object} object
     * @returns {boolean} - Returns true if all mandatory elements are visible on page; fails assertion otherwise 
     */
    async checkPage(object = null) {
        if (object == null) object = this
        for (var element in object) {
            if (object[element].hasOwnProperty('isMandatory')) {
                if (object[element].isMandatory()) {
                    await t.expect(object[element].visible)
                        .ok(`Expecting element ${element} to be visible on page ${object.constructor.name}, but it isn't
                            \nExecution failed at URL: ${await utils.getURL()}`)
                }
            }
            if ({}.toString.call(object[element]) == '[object Object]'){
                this.checkPage(object[element])
            }
        }
        return true
    }

    async set_language(language) {
        await t
            .click(this.language.withText(language.toUpperCase()));
        t.ctx.language = language;
    }

    async navigate() {
        await t
            .navigateTo(this.url)
    }
}

export default Base;
