import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class MigrationFilesUpload extends Base {
    constructor() {
        super()
        this.url = `${data.url}/company/migration-files`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Migration files upload',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new MigrationFilesUpload()