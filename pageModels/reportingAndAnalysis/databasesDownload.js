import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class DatabasesDownload extends Base {
    constructor() {
        super()
        this.url = `${data.url}/reporting-and-analysis/databases-download`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Databases download',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new DatabasesDownload()