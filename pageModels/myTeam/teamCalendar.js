import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class TeamCalendar extends Base {
    constructor() {
        super()
        this.url = `${data.url}/team/calendar2`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Team Calendar',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
  
        this.reload = Selector('.btn-reload-data-grid').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Reload data',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new TeamCalendar()