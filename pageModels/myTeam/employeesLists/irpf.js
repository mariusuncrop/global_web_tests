import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class IRPF extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/irpf`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single',
                '.table-filters .row:nth-of-type(2) .Select--single');
    }
}

export default new IRPF()
