import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Pensions extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/pension-funds2`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new Pensions()
