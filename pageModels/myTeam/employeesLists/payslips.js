import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Payslips extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/payslips`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new Payslips()
