import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from '../../base'
import navigation from './navigation'

class EmployeesListBase extends Base {
    constructor(url, title, filterEmployees, filterLEs) {
        super();
        this.url= url;
        this.navigation = navigation;
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: title,
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        });  
        this.downloadXLSX = Selector('#download-as-excel-button-div button').addCustomMethods({
            isMandatory: () => true
        });
        this.reload = Selector('.btn-reload-data-grid').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Reload data',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        });
        this.search = Selector("input[data-for='search-tooltip']").addCustomMethods({
            isMandatory: () => true
        });
        if (filterEmployees) this.filterEmployees = Selector(filterEmployees).addCustomMethods({
            isMandatory: () => true
        });
        if (filterLEs) this.filterLEs = Selector(filterLEs).addCustomMethods({
            isMandatory: () => true
        });
        this.table = Selector('table.table').addCustomMethods({
            isMandatory: () => true
        })
    }
}

export default EmployeesListBase