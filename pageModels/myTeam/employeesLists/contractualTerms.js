import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class ContractualTerms extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/contributions`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}
export default new ContractualTerms()