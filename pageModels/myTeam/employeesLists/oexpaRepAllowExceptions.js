import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class OEXPARepAllowExceptions extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/oexpa-rep-exc`,
                'Employees lists');
    }
}

export default new OEXPARepAllowExceptions()
