import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class OEXPARepAllow extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/oexpa-rep-all`,
                'Employees lists');
    }
}

export default new OEXPARepAllow()
