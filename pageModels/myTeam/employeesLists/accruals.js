import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Accruals extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/accruals`,
                'Accruals');
    }
}

export default new Accruals()
