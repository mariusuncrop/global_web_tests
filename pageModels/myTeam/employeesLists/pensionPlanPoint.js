import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class PensionPlanPoint extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/pension-plan-points`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new PensionPlanPoint()
