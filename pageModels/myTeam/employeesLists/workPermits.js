import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class WorkPermits extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/work-permits`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new WorkPermits()
