import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class PTExtrapay extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/portugal-extrapay`,
                'PT Extrapay',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new PTExtrapay()
