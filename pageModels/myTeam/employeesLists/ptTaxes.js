import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class PTTaxes extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/portugal-data`,
                'PT Taxes',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new PTTaxes()
