import {Selector, t} from 'testcafe'
import data from '../../../config'
import EmployeesListBase from './employeesListBase'

class AcademicData extends EmployeesListBase {
    constructor() {
        super(`${data.url}/team/employee-lists/irpf`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new AcademicData()
