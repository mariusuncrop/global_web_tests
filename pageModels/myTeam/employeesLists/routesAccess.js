import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class RoutesAccess extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/routes-access`,
                'ROUTES-ACCESS',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new RoutesAccess()
