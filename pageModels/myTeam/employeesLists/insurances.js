import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Insurances extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/insurances`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new Insurances()