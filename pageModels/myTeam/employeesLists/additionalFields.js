import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class AdditionalFields extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/additional-fields`,
                'Employees lists',
                null,
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new AdditionalFields()
