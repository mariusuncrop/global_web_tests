import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Hierarchies extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/hierarchies`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new Hierarchies()