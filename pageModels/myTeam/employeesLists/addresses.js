import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Addresses extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/addresses`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single',
                '.table-filters .row:nth-of-type(2) .Select--single');
    }
}

export default new Addresses()