import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'

class Calendars extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/calendars`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single');
    }
}

export default new Calendars()