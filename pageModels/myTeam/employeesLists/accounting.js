import {Selector, t} from 'testcafe'
import data from '../../../config'
import Base from './employeesListBase'
import navigation from './navigation'

class Accounting extends Base {
    constructor() {
        super(`${data.url}/team/employee-lists/accounting`,
                'Employees lists',
                '.table-filters .row:nth-of-type(1) .Select--single'
                );
    }
}

export default new Accounting()