import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class ExpenseInvoiced extends Base {
    constructor() {
        super()
        this.url = `${data.url}/team/expense-invoiced`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Expense Invoiced',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new ExpenseInvoiced()
