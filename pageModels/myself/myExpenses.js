import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class MyExpenses extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/expenses`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'My Expenses',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.reload = Selector('.panel-title button.btn-reload-data-grid').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Reload data',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        
    }
}

export default new MyExpenses()