import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class MyClockings extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/clocking`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Clocking',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.clockIn = Selector('.panel-body  .btn-success.big').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Clock in',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.clockOut = Selector('.panel-body  .btn-danger.big').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Clock out',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new MyClockings()
