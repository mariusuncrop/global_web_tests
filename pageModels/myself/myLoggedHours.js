import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'
import utils from '../../utils/utils'

class MyLoggedHours extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/log-hours`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'My Logged Hours',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new MyLoggedHours()
