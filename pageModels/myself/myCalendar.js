import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'
import utils from '../../utils/utils'

class MyCalendar extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/calendar2`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'My Calendar',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.timesheet = Selector('.panel-body a[href="/myself/my-time-sheet/"]').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Time sheet',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.downloadExcel = Selector('.panel-body button.green-button').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: `Download ${utils.getCurrentMonthName()} Data (Excel)`,
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            })
        })
        this.downloadPDF = Selector('.panel-body button.red-button').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: `Download ${utils.getCurrentMonthName()} Data (PDF)`,
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            })
        })
    }
}

export default new MyCalendar()
