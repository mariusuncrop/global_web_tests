import { Selector, t } from 'testcafe';
import data from '../../config'
import Base from '../base'

class MyPersonalData extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/personal`
        this.title = Selector('.panel-title > div.pull-left')
        this.reload = Selector('.panel-title #reload-data-button-div button')
        this.addresses = {
            title: Selector('.panel-adminme').nth(0).find('.panel-title > div.pull-left').addCustomMethods({
                getExpectedValue: () => ({
                    cat: '',
                    de: '',
                    en: 'Addresses',
                    es: '',
                    fr: '',
                    it: '',
                    jp: '',
                    pt: '',
                }),
                isMandatory: () => true
            }),
            reload: Selector('.panel-adminme').nth(0).find('.panel-title #reload-data-button-div button').addCustomMethods({
                getExpectedValue: () => ({
                    cat: '',
                    de: '',
                    en: 'Reload data',
                    es: '',
                    fr: '',
                    it: '',
                    jp: '',
                    pt: '',
                })
            })
        },
        this.banks = {
            title: Selector('.panel-adminme').nth(1).find('.panel-title > div.pull-left').addCustomMethods({
                getExpectedValue: () => ({
                    cat: '',
                    de: '',
                    en: 'Banks',
                    es: '',
                    fr: '',
                    it: '',
                    jp: '',
                    pt: '',
                }),
                isMandatory: () => true
            }),
            reload: Selector('.panel-adminme').nth(1).find('.panel-title #reload-data-button-div button').addCustomMethods({
                getExpectedValue: () => ({
                    cat: '',
                    de: '',
                    en: 'Reload data',
                    es: '',
                    fr: '',
                    it: '',
                    jp: '',
                    pt: '',
                })
            })
        },
        this.dependents = {
            title: Selector('.panel-adminme').nth(2).find('.panel-title > div.pull-left').addCustomMethods({
                getExpectedValue: () => ({
                    cat: '',
                    de: '',
                    en: 'Dependents',
                    es: '',
                    fr: '',
                    it: '',
                    jp: '',
                    pt: '',
                }),
                isMandatory: () => true
            }),
            reload: Selector('.panel-adminme').nth(2).find('.panel-title #reload-data-button-div button').addCustomMethods({
                getExpectedValue: () => ({
                    cat: '',
                    de: '',
                    en: 'Reload data',
                    es: '',
                    fr: '',
                    it: '',
                    jp: '',
                    pt: '',
                })
            })
        }
    }
}
export default new MyPersonalData()
