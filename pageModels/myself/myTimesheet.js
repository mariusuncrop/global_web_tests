import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class MyTimesheet extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/my-time-sheet`
        this.title = Selector('.panel-title > div.pull-left').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'My Time-sheet',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.reload = Selector('.panel-title #reload-data-button-div button').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Reload data',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new MyTimesheet()