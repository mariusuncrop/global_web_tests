import {Selector, t} from 'testcafe'
import data from '../../config'
import Base from '../base'

class MyObjectives extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/objectives`
        this.title = Selector('.panel-title > div:first-of-type').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'My Objectives',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.reload = Selector('.panel-title #reload-data-button-div button').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Reload data',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.download = Selector('#download-as-excel-button-div button').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Dwonload as .XLSX',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
    }
}

export default new MyObjectives()
