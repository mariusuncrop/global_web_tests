import { Selector, t } from 'testcafe';
import Base from '../base'
import data from '../../config';

class MyDocuments extends Base {
    constructor() {
        super()
        this.url = `${data.url}/myself/documents`;
        this.title = Selector('.panel-title > div:first-of-type').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'My Documents',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.reload = Selector('.panel-title div #reload-data-button-div button').addCustomMethods({
            getExpectedValue: () => ({
                cat: '',
                de: '',
                en: 'Reload data',
                es: '',
                fr: '',
                it: '',
                jp: '',
                pt: '',
            }),
            isMandatory: () => true
        })
        this.search = Selector('.panel-heading input')
    }
}

export default new MyDocuments()