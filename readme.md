# Global Automation Tests

Tests written using [Testcafe](https://devexpress.github.io/testcafe/) - A node.js tool to automate end-to-end web testing

# Prerequisites

[Node.js](https://nodejs.org/en/)
[npm](https://www.npmjs.com/)

# Installation

All dependencies will be installed by running

```
npm install 
```

# Project Structure
```
project
|-- pageModels        <!--- Page models (structured according to the UI menus) --->
|   -- myself 
|   -- myTeam
        -- employeesLists
            -- calendars.js
        -- actionCenter.js
    -- payroll
|-- tests         <!--- Test scenarios --->
|   -- login.js
    -- generic
        -- checkPages.js
    -- home
        -- homePage.js
|-- config.js <!--- Configuration files; stores environment details --->
|-- .testcaferc.json <!--- Testcafe configuration file
|-- bitbucket-pipelines.yml
|-- .testrailrc <!--- Testrail configuration file --->
|-- package.json
|-- readme.md
```
# Running tests

To run tests:

```
testcafe
```
See [testcafe documentation](https://devexpress.github.io/testcafe/documentation/guides/basic-guides/run-tests.html) for run parameters;

