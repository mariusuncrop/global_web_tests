import data from '../../config'
import adminUser from '../../utils/roles'
import * as pages from '../../pageModels/pages'

fixture(`Test home page`)
    .beforeEach(async t => {
        await t
            .useRole(adminUser)
    })

test
    ('Check home page ok', async t => {
        console.log (await pages.home.bad.isMandatory())
        console.log('@@@@@@@@@@@@@@@@@@@@@@@\n\n\n\n\n\n')
        var startDate = new Date()
        await pages.home.checkPage()
        var endDate = new Date()
        console.log(`\n\n\n\n\n\n\n    Total seconds for checking the page:     ${(endDate-startDate)/1000}`)
        await t
            .navigateTo(pages.home.url)
            .expect(pages.home.uploadFile.textContent).eql((await pages.home.uploadFile.getExpectedValue())['en'])
    })

test
    ('Check home page bug', async t => {
        await t
            .navigateTo(pages.home.url)
            .expect(pages.home.uploadFile.textContent).eql('test')
    })

test
    ("Test", async t => {
        await t
            .navigateTo(pages.myTeam.employeesLists.referenceLetter.url)
            .expect(pages.myTeam.employeesLists.referenceLetter.title.textContent).contains('test')
    })

