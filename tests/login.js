import data from '../config'
import * as pages from '../pageModels/pages'
import * as utils from '../utils/utils'

fixture(`Test login page`)

test
    .meta({CID: 'C51671'})
    .page(`${pages.login.url}`)
    ('Test successfully login', async t => {
        await
            pages.login.auth(`${data.defaultAdminEmail}`, `${data.defaultAdminPassword}`);
        await t
            .expect(pages.home.header.logout.visible).ok("Expecting to see the logout button, but it isn't visible")
    })

test
    .meta({CID: 'C51672'})
    .page(`${pages.login.url}`)
    ('Check mandatory fields validation', async t => {
        await t
            .click(pages.login.submit)
            .expect(pages.login.submit.textContent)
            .eql((await pages.login.submit.getExpectedValue())['en'])
            .expect(pages.login.email.error_message.textContent)
            .eql((await pages.login.email.error_message.getExpectedValue())['en'])
            .expect(pages.login.password.error_message.textContent)
            .eql((await pages.login.password.error_message.getExpectedValue())['en']);
    })

test
    .meta({CID: 'C51673'})
    .page(`${pages.login.url}`)
    ('Check login with deactivated user', async t => {
        await pages.login.auth(data.inactiveUserEmail, data.inactiveUserPassword)
        await t
            .expect(pages.login.toasts.error.title.textContent).eql("Error")
            .expect(pages.login.toasts.error.text.textContent).eql("This account was deactivated")
    })

test
    .meta({CID: 'C51674'})
    .page(`${pages.login.url}`)
    ('Check login with wrong email', async t => {
        await pages.login.auth('wrong@email.com', data.defaultAdminPassword)
        await t
            .expect(pages.login.toasts.error.title.textContent).eql("Error")
            .expect(pages.login.toasts.error.text.textContent).eql("User or password are incorrect")
    })

test
    .meta({CID: 'C51675'})
    .page(`${pages.login.url}`)
    ('Check login with wrong password', async t => {
        await pages.login.auth(data.defaultAdminEmail, 'WrongPass123')
        await t
            .expect(pages.login.toasts.error.title.textContent).eql("Error")
            .expect(pages.login.toasts.error.text.textContent).eql('User or password are incorrect')
    })

test
    .meta({CID: 'C51676'})
    .page(`${pages.login.url}`)
    ('Check forgot password', async t => {
        await t
            .click(pages.login.forgotPassword)
            .expect(pages.forgotPassword.title.textContent)
            .eql((await pages.forgotPassword.title.getExpectedValue())['en'])
            .expect(pages.forgotPassword.subtitle.textContent)
            .eql((await pages.forgotPassword.subtitle.getExpectedValue())['en'])
            .expect(pages.forgotPassword.reset.hasAttribute('disabled')).ok()
            .typeText(pages.forgotPassword.input, `${data.defaultAdminEmail}`)
            .expect(pages.forgotPassword.reset.hasAttribute('disabled')).notOk()
            .click(pages.forgotPassword.reset)
            .expect(pages.forgotPassword.done.textContent)
            .eql((await pages.forgotPassword.done.getExpectedValue())['en']);
    })

const languages = ['cat', 'en', 'es', 'de', 'fr', 'it', 'pt', 'jp']
languages.forEach(lang => {
    test
        .meta({CID: 'C51677'})
        .page(`${pages.login.url}`)
        (`Check text translations for language ${lang}`, async t => {
            await pages.login.set_language(lang);
            await t
                .expect(pages.login.submit.textContent)
                .eql((await pages.login.submit.getExpectedValue())[lang])
                .expect(pages.login.forgotPassword.textContent)
                .eql((await pages.login.forgotPassword.getExpectedValue())[lang])
        });
});

test
    .meta({CID: 'C51678'})
    .page(pages.login.url)
    ('Check accesing private page before login', async t => {
        await pages.home.navigate()
        await t.expect(utils.getURL()).eql(pages.login.url)
    })
