import data from '../../config'
import adminUser from '../../utils/roles'
import * as pages from '../../pageModels/pages'
import CustomRequestHook from '../../utils/customRequestHook'
import { Selector, RequestLogger } from 'testcafe';

const logger = RequestLogger(null, {
    includeHeaders: true,
    includeBody: true,
    logResponseHeaders: true,
    logResponseBody:    true
});

fixture(`Navigate through pages and check for errors`)
    .requestHooks(logger)
    .beforeEach(async t => {
        await t
            .useRole(adminUser)
    })
    .afterEach(async t => {
        for (const request of logger.requests) {
            if (request.response) {
                // console.log(`request to ${request.request.url}       ${request.response.statusCode}`)
                await t
                    .expect(request.response.statusCode).lt(400, `Request to ${request.request.url} failed with code ${request.response.statusCode}.\nReponse body was ${request.response.body}`)
            }
        };
    });

var scenarios = [
    {
        "myself": ["myActivityReport:C51718", "myCalendar:C51719", "myClockings:C51720", "myDocuments:C51721", "myExpenses:C51722", "myLoggedHours:C51723", "myObjectives:C51724", "myPersonalData:C51725", "myPlanning:C51726", "myReceivedRequests:C51727", "myTimesheet:C51728"]
    },
    {
        "myTeam": ["employeesFolders:C51729", "teamOverview:C51730", "directAbsences:C51731", "compensation:C51732", "actionCenterOld:C51733", "actionCenter:C51734", "activityReport:C51735", "teamCalendar:C51736", "teamMutations:C51737", "expenseInvoiced:C51738", "absenceDetails:C51739", "vacationDetails:C51740", "countersDetails:C51741", "objectives:C51742", "pendingApprovals:C51743", "expenses:C51744"]
    },
    {
        "payroll": ["payrollRuns:C51745", "payrollInput:C51746", "payrollOutput:C51747", "payrollOutput_:C51748", "payrollDeclaration:C51749", "icpCenter:C51750", "payrollSignOff:C51751", "payrollControls:C51752", "checkList:C51753", "taxRates:C51754", "uploadedFiles:C51755", "interfaceFiles:C51756", "calculationLogs:C51757", "calculationOutput:C51758", "accountingOutput:C51759", "paySubperiodOutput:C51760", "employeeTaxOutput:C51761", "ptOutputBases:C51762", "inputInterface:C51763", "modulesManager:C51764"]
    },
    {
        "timeAndExpenses": ["sftpConnections:C51765", "timeGroupsSetup:C51766", "timeApprovers:C51767", "countersSetup:C51768", "absencesSetup:C51769", "calendarsSetup:C51770", "closedPeriods:C51771", "expenseGroupSetup:C51772", "expenseApprovers:C51773", "clockingSetup:C51774", "timeMailConfig:C51775"]
    },
    {
        "accounts": ["expenses:C51776", "billing:C51777", "transactions:C51778", "transactionsSetup:C51779", "reportGeneration:C51780", "exchangeSetup:C51781", "cashCall:C51782"]
    },
    {
        "customerService": ["ticketingSystem:C51783"]
    },
    {
        "legalEntity": ["legalEntityFolders:C51784", "legalEntityLists:C51785", "insurances:C51786", "historicalFilesUpload:C51787", "multipleFilesUpload:C51788", "migrationFilesUpload:C51789"]
    },
    {
        "client": ["clientFolders:C51790", "clientLists:C51791"]
    },
    {
        "legislation": ["legislationFolders:C51792", "legislationLists:C51793", "insurances:C51794", "setup:C51795"]
    },
    {
        "crm": ["accounts:C51796", "opportunities:C51797", "activities:C51798", "stakeholders:C51799", "scopes:C51800", "seoTargets:C51801", "seoAchievement:C51802", "commercialDocuments:C51803"]
    },
    {
        "reportingAndAnalysis": ["databasesDownload:C51804", "usersManagAndControl:C51805"]
    },
    {
        "helpAndDocumentation": ["policiesAndTraining:C51806", "manualsAndGuidelines:C51807", "operationalDocuments:C51808", "manageFiles:C51809", "securityManagement:C51810"]
    },
    {
        "configuration": ["tags:C51811", "logs:C51812", "users:C51813", "roles:C51814", "wageTypes:C51815", "worker:C51816", "mutations:C51817", "notifications:C51818", "parameters:C51819", "logsByEmployee:C51820"]
    }
]

// scenarios = [
//     {
//         "myTeam": ["employeesLists", "expenseInvoiced"]
//     },
//     {
//         "legislation": ["legislationFolders"]
//     }
// ]

scenarios.forEach(scenario => {
    var scenarioMenu = Object.keys(scenario)[0]
    var scenarioPages = scenario[scenarioMenu]
    scenarioPages.forEach(scenarioPage => {
        var page = scenarioPage.split(':')[0]
        var tcID = scenarioPage.split(':')[1]
        test
            .meta({CID: tcID})
            .page(pages.home.url)
            (`Navigate to page ${scenarioMenu} -> ${page} and check that page is loaded correctly`, async t => {
                await t
                    .click(pages['home']['sidebar'][scenarioMenu]['button'])
                    .click(pages.home.sidebar[scenarioMenu][page]);
                
                await pages[scenarioMenu][page].checkPage()
            })
    })
})
