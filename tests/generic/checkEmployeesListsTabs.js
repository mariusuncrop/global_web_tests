import data from '../../config'
import adminUser from '../../utils/roles'
import * as pages from '../../pageModels/pages'
import { Selector, RequestLogger } from 'testcafe';

const logger = RequestLogger(null, {
    includeHeaders: true,
    includeBody: true,
    logResponseHeaders: true,
    logResponseBody:    true
});

fixture(`Navigate through Employees Lists tabs and check for errors`)
    .requestHooks(logger)
    .beforeEach(async t => {
        await t
            .useRole(adminUser)
    })
    .afterEach(async t => {
        for (const request of logger.requests) {
            if (request.response) {
                // console.log(`request to ${request.request.url}       ${request.response.statusCode}`)
                await t
                    .expect(request.response.statusCode).lt(400, `Request to ${request.request.url} failed with code ${request.response.statusCode}.\nReponse body was ${request.response.body}`)
            }
        };
    });

var employeesListsTabs = ['overview:C51681', 'accounting:C51682', 'addresses:C51683', 'banks:C51684', 'calendars:C51685', 'contracts:C51686', 'contributions:C51687', 'contractualTerms:C51688', 'dependents:C51689', 'garnishments:C51690', 'groups:C51691', 'hierarchies:C51692', 'insurances:C51693', 'irpf:C51694', 'langsAndIDs:C51695', 'chTaxes:C51696', 'mutations:C51697', 'pensions:C51698', 'referenceLetter:C51699', 'universalData:C51700', 'timeGroups:C51701', 'expenseGroupSetup:C51702', 'payslips:C51703', 'workPermits:C51704', 'ptTaxes:C51705', 'ptExtrapay:C51706', 'oexpaRepAllow:C51707', 'oexpaRepAllowExceptions:C51708', 'additionalFields:C51709', 'routesAccess:C51710', 'accruals:C51711', 'providers:C51712', 'academicData:C51713', 'pensionPlanPoint:C51714', 'otherEmployment:C51715']
employeesListsTabs.forEach(values => {
    var employeeListstab = values.split(':')[0]
    var testCaseId = values.split(':')[1]
    test
    .meta({CID: testCaseId})
    .page(pages.home.url)
    (`Navigate to Employess Lists -> ${employeeListstab} tab and check that the page is loaded correctly`, async t => {
        await t
            .click(pages.home.sidebar.myTeam.button)
            .click(pages.home.sidebar.myTeam.employeesLists);
        if (employeeListstab != 'overview')
            await t.click(pages.myTeam.employeesLists.navigation[employeeListstab])
        await pages.myTeam.employeesLists[employeeListstab].checkPage();
    })
})
